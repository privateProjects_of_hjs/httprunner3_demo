# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年04月16日  16点33分
@contactInformation: 727803257@qq.com
@Function: 对一个明文数据进行rsa加密，并得到一个加密的密码
"""

# 请从这行往下开始编写脚本

import rsa
import base64
from utils.getPath import GetPath
from utils.get_env_variables import get_env_variables

getPath = GetPath()
env_variables = get_env_variables()

# 第1步：获取存放rsa公钥数据的这个文件的绝对路径
path_of_public_key_of_rsa = getPath.get_absolute_path_of_one_resource(secondFloorFileName="rsaKey",
                                                                      resourceName=env_variables[
                                                                          "pemNameOfPublicKeyOfRsa"])


class RsaEncryption:
    """对一个明文密码进行加密，得到一个被加密的密文密码"""

    @classmethod
    def encryption(cls, password):
        """
        :param password 一个没被加密的密码/一个明文密码，数据类型为str
        :return encryptedPassword   一个加密的密码/一个密文密码，数据类型为str

        # 细节：
        # --》1.英文"encryption"的含义：加密
        # --》2.英文"encrypted"的含义：加密的
        # --》3.英文"encrypt"的含义：把某个数据进行加密
        """

        # 第1步：对每个入参的入参值进行指定的规则校验和数据处理
        if isinstance(password, int):
            password = str(password).encode("utf8")
        elif isinstance(password, str):
            password = password.encode("utf8")

        # 第2步：打开存放rsa公钥数据的这个文件，并得到rsa公钥数据
        with open(path_of_public_key_of_rsa, mode="rb") as pub:
            publicKey = pub.read()

        # 第3步：调用rsa库提供的方法，得到一个公钥对象
        publicKeyObject = rsa.PublicKey.load_pkcs1_openssl_pem(publicKey)

        # 第4步：调用rsa库提供的方法，得到一个加密的密码，数据类型为byte
        encryptedPassword = rsa.encrypt(password, publicKeyObject)

        # 第5步：调用base64库提供的方法，对一个加密的密码进行解码，得到一个数据类型为str的加密的密码
        encryptedPassword = base64.b64encode(encryptedPassword).decode()

        # 第6步：返回一个被解码后的数据类型为str的加密的密码
        return encryptedPassword


if __name__ == "__main__":
    rsaEncryption = RsaEncryption()
    rsaEncryption.encryption("aa123456")

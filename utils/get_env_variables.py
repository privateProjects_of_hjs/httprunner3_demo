# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年10月18日  10点22分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本

from typing import Dict
from utils.getPath import GetPath

getPath = GetPath()

# 第1步：获取存放当前运行环境的环境配置文件
path_of_env = getPath.get_absolute_path_of_one_resource(resourceName=".env")


def get_env_variables() -> Dict:
    """

    :return env_variables_mapping 返回当前运行环境的环境信息，数据类型为dict
            返回值比如为：
                {
                "UserName": "debugtalk",
                "Password": "123456",
                "PROJECT_KEY": "ABCDEFGH"
                }
    """
    env_variables_mapping = {}

    with open(path_of_env, mode="rb") as fp:
        for line in fp:
            if b"=" in line:
                variable, value = line.split(b"=", 1)
            elif b":" in line:
                variable, value = line.split(b":", 1)
            else:
                raise Exception(".env format error")

            env_variables_mapping[
                variable.strip().decode("utf-8")
            ] = value.strip().decode("utf-8")
    return env_variables_mapping


if __name__ == "__main__":
    get_env_variables()

# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年10月20日  16点40分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本

import pytest

if __name__ == "__main__":
    # pytest.main(["-v", "--alluredir","./allure_report","--clean-alluredir"])
    # pytest.main(["-v", "--alluredir","./allure_report"])
    # pytest.main(["-v", "-m smoke", "--alluredir","./allure_report","--clean-alluredir"])

    # pytest.main(["-v", "-m smoke and formal", "--alluredir","./allure_report","--clean-alluredir"])
    pytest.main(["-v", "-m smoke or formal", "--alluredir","./allure_report","--clean-alluredir"])
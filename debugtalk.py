from utils.common import Common
from utils.rsaEncryption import RsaEncryption

common = Common()
rsaEncryption = RsaEncryption()


def userAgent():
    """
    :return value  返回一个请求头信息，数据类型为str
    """
    value = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36"
    return value


def encryption(password):
    """

    :param password: 明文密码
    :return: 密文密码
    """
    if password == "":
        encryptedPassword = ""
    else:
        encryptedPassword = rsaEncryption.encryption(password)
    return encryptedPassword


def nonexistentAccount():
    """
    :return 返回一个不存在的运营后台登录账号
    # 细节：
    # --》1.英文"nonexistentAccount"的含义：不存在的账号
    """
    # 第1步：获取一个随机账号
    accountName = common.randomName("账号", 6)

    # 因为我后续都放弃通过数据库来获取数据，所以相关的代码被我都注释掉了
    # # 第2步：获取指定表里表字段'account_name'的所有数据
    # data = petMysqlConnection.query_of_sql("select account_name,id from sys_operate_account")
    # # 第3步：进行递归校验，校验该随机账号不存在该数据表
    # accountNames = []
    # for i in data:
    #     for key, value in i.items():
    #         if "account_name" == key:
    #             accountNames.append(value)
    # if accountName not in accountNames:
    #     return accountName
    # else:
    #     finalAccountName = nonexistentAccount()
    #     return finalAccountName

    # 第2步：通过"获取所有账号信息"的接口，我暂时还没调试，所以先不校验生成的随机账号accountName的值是否存在就先默认当做不存在(因为存在的概率为200亿分之1)
    return accountName


def nonexistentPassWord():
    """
    :return 返回一个不存在的运营后台登录账号所对应的密码
    # 细节：
    # --》1.英文"nonexistentPassWord"的含义：不存在的密码
    """
    # 第1步：获取一个随机账号
    # 细节：
    # -->1.只有20亿分之1的概率会密码重复，所以直接造密码即可，不需要跟对应数据表里的密码进行对比
    password = common.randomName("c6", 6)
    return password


def getUserLevelInformation(userLevelList, data: str):
    """
    :param userLevelList 用户等级列表信息
    :param data 数据
    :return 返回想要的数据
    """

    # 第1步：对每个入参的入参值进行指定的规则校验和数据处理
    if data not in ["nextLevel", "nextMin", "nextMax", "maxIds", "maxRank"]:
        raise Exception("入参data的入参值有误")

    ranks = []  # 所有已存在的等级的汇总
    mins = []  # 所有已存在的每个等级里的最小值的汇总
    maxs = []  # 所有已存在的每个等级里的最大值的汇总
    ids = []  # 所有已存在的等级id的汇总
    # 如果查不到等级时
    if userLevelList is None:
        nextLevel = 1
        nextMin = 1
        nextMax = 2
    else:

        # 如果有查到等级时
        for i in userLevelList:
            for key, value in i.items():
                if key == "rank":
                    ranks.append(value)
                if key == "min":
                    mins.append(value)
                if key == "max":
                    maxs.append(value)
                if key == "id":
                    ids.append(value)
        # 下一个等级的值
        nextLevel = max(ranks) + 1
        # 下一个等级的最小分数值
        nextMin = max(maxs) + 1
        # 下一个等级的最大分数值
        nextMax = max(maxs) + 2
        # 目前最高等级的等级id
        maxIds = max(ids)
        # 目前最高等级的值
        maxRank = max(ranks)

    if data == "nextLevel":
        return nextLevel

    if data == "nextMin":
        return nextMin

    if data == "nextMax":
        return nextMax
    if data == "maxIds":
        return maxIds
    if data == "maxRank":
        return maxRank


def getTempleLevelInformation(templeLevelList, data: str):
    """
    :param templeLevelList 空间等级列表信息
    :param data 数据
    :return 返回想要的数据
    """

    # 第1步：对每个入参的入参值进行指定的规则校验和数据处理
    if data not in ["nextLevel", "nextMin", "nextMax", "maxIds", "maxRank"]:
        raise Exception("入参data的入参值有误")

    ranks = []  # 所有已存在的等级的汇总
    mins = []  # 所有已存在的每个等级里的最小值的汇总
    maxs = []  # 所有已存在的每个等级里的最大值的汇总
    ids = []  # 所有已存在的等级id的汇总
    # 如果查不到等级时
    if templeLevelList is None:
        nextLevel = 1
        nextMin = 1
        nextMax = 2
    else:

        # 如果有查到等级时
        for i in templeLevelList:
            for key, value in i.items():
                if key == "rank":
                    ranks.append(value)
                if key == "min":
                    mins.append(value)
                if key == "max":
                    maxs.append(value)
                if key == "id":
                    ids.append(value)
        # 下一个等级的值
        nextLevel = max(ranks) + 1
        # 下一个等级的最小分数值
        nextMin = max(maxs) + 1
        # 下一个等级的最大分数值
        nextMax = max(maxs) + 2
        # 目前最高等级的等级id
        maxIds = max(ids)
        # 目前最高等级的值
        maxRank = max(ranks)

    if data == "nextLevel":
        return nextLevel

    if data == "nextMin":
        return nextMin

    if data == "nextMax":
        return nextMax
    if data == "maxIds":
        return maxIds
    if data == "maxRank":
        return maxRank


def intToStr(value: int):
    """

    :param value: 一个数据类型为int的值
    :return:
    """
    data = str(value)
    return data


def getUserInformation(userInformation: dict, enum: str):
    """
    :param userInformation 用户信息
    :param enum 枚举类
    :return 返回想要的数据
    """

    if enum not in ["mobile", "accountName", "currTenantId", "currTenantUserId", "userNickName"]:
        raise Exception("入参enum的入参值有误")

    data = {}
    for key1, value1 in userInformation.items():
        if key1 == "mobile":
            data[key1] = value1
        if key1 == "accountName":
            data[key1] = value1
        if key1 == "enterpriseUserInfoVo":
            for key2, value2 in value1.items():
                if key2 == "currTenantId":
                    data[key2] = value2
                if key2 == "currTenantUserId":
                    data[key2] = value2
                if key2 == "userNickName":
                    data[key2] = value2
    return data[enum]


if __name__ == "__main__":
    pass

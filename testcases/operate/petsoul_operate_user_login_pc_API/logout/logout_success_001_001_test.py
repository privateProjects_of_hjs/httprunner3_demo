# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年09月29日  17点38分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本
import pytest
import allure
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase

@allure.epic("宠物空间项目-运营后台")
@allure.feature("自运营账号登录接口")
@allure.story("pc端退出登录")
@pytest.mark.smoke
@pytest.mark.formal
class TestCaseLogoutSuccess001001(HttpRunner):
    config = (
        Config("退出运营后台成功")
            .base_url("${ENV(baseUrlOfOperate)}")
            .variables(**{})
            .verify(True)
            .export(*[])
    )

    teststeps = [

        Step(
            RunRequest("登录运营后台成功(使用第一个账号登录)")
                .post("/fapig/calendar/day.php")
                .with_headers(**{})
                .with_params(
                **{
                    "date": "2021-05-01",
                    "detail": 1,
                    "key": "3998cbe7f1dd91357156d35a81d837f4"
                }

            )
                .extract()
                .with_jmespath("body.result", "result")
                .validate()
                .assert_equal("status_code", 200)
                .assert_equal("body.reason", "success")
                .assert_length_equal("body.result.date", 10)
        )
    ]


if __name__ == "__main__":
    TestCaseLogoutSuccess001001().test_start()

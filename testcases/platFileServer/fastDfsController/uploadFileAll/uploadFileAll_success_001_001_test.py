# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年09月29日  17点38分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本
import pytest
import allure
from httprunner import HttpRunner, Config, Step, RunRequest



@allure.epic("宠物空间项目-平台文件服务")
@allure.feature("文件资源控制器")
@allure.story("上传图片资源")
@pytest.mark.smoke
class TestCaseUploadFileAllSuccess001001(HttpRunner):

    config = (
        Config("上传一张小于100kb(实际大小是93kb)的png格式的图片到资源服务器成功")
            .base_url("${ENV(baseUrlOfPlatFileServer)}")
            .variables(**{})
            .verify(True)
            .export(*["relativePathOfResource001001"])
    )

    teststeps = [

        Step(
            RunRequest("登录运营后台成功(使用第一个账号登录)")
                .post("/fapig/calendar/day.php")
                .with_headers(**{})
                .with_params(
                **{
                    "date": "2021-05-01",
                    "detail": 1,
                    "key": "3998cbe7f1dd91357156d35a81d837f4"
                }

            )
                .extract()
                .with_jmespath("body.result", "result")
                .validate()
                .assert_equal("status_code", 200)
                .assert_equal("body.reason", "success")
                .assert_length_equal("body.result.date", 10)
        )
    ]



if __name__ == "__main__":
    TestCaseUploadFileAllSuccess001001().test_start()
